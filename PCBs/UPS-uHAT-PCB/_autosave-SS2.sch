EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 8150 7650 0    50   ~ 0
02/06/2021
$Comp
L pspice:CAP C4
U 1 1 60BA5715
P 7150 5450
F 0 "C4" H 7328 5496 50  0000 L CNN
F 1 "10uF" H 7328 5405 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 7150 5450 50  0001 C CNN
F 3 "~" H 7150 5450 50  0001 C CNN
	1    7150 5450
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C5
U 1 1 60BA5A69
P 7900 5800
F 0 "C5" H 8078 5846 50  0000 L CNN
F 1 "10uF" H 8078 5755 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 7900 5800 50  0001 C CNN
F 3 "~" H 7900 5800 50  0001 C CNN
	1    7900 5800
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C6
U 1 1 60C372E3
P 9050 5650
F 0 "C6" H 9228 5696 50  0000 L CNN
F 1 "150uF" H 9228 5605 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D7.5mm_P2.50mm" H 9050 5650 50  0001 C CNN
F 3 "~" H 9050 5650 50  0001 C CNN
	1    9050 5650
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LT1117-3.3 U2
U 1 1 60C372E4
P 7750 5050
F 0 "U2" H 7750 5292 50  0000 C CNN
F 1 "LT1117-3.3" H 7750 5201 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 7750 5050 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/1117fd.pdf" H 7750 5050 50  0001 C CNN
	1    7750 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R4
U 1 1 60C372E5
P 8500 5300
F 0 "R4" H 8568 5346 50  0000 L CNN
F 1 "633Ω" H 8568 5255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 8540 5290 50  0001 C CNN
F 3 "~" H 8500 5300 50  0001 C CNN
	1    8500 5300
	1    0    0    -1  
$EndComp
$Comp
L Simulation_SPICE:DIODE D2
U 1 1 60BAA080
P 7750 4400
F 0 "D2" H 7750 4183 50  0000 C CNN
F 1 "1N4148" H 7750 4274 50  0000 C CNN
F 2 "Diode_THT:D_5W_P10.16mm_Horizontal" H 7750 4400 50  0001 C CNN
F 3 "~" H 7750 4400 50  0001 C CNN
F 4 "Y" H 7750 4400 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "D" H 7750 4400 50  0001 L CNN "Spice_Primitive"
	1    7750 4400
	-1   0    0    1   
$EndComp
$Comp
L Device:R_US R5
U 1 1 60BAA700
P 8500 5700
F 0 "R5" H 8568 5746 50  0000 L CNN
F 1 "1kΩ" H 8568 5655 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 8540 5690 50  0001 C CNN
F 3 "~" H 8500 5700 50  0001 C CNN
	1    8500 5700
	1    0    0    -1  
$EndComp
$Comp
L Device:Battery BT2
U 1 1 60BB42D0
P 6350 4850
F 0 "BT2" H 6458 4896 50  0000 L CNN
F 1 "Vin-5V-Li+-battery" H 6458 4805 50  0000 L CNN
F 2 "Connector_Molex:Molex_CLIK-Mate_502382-0270_1x02-1MP_P1.25mm_Vertical" V 6350 4910 50  0001 C CNN
F 3 "~" V 6350 4910 50  0001 C CNN
	1    6350 4850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 60BC3DAC
P 7900 6300
F 0 "#PWR07" H 7900 6050 50  0001 C CNN
F 1 "GND" H 7905 6127 50  0000 C CNN
F 2 "" H 7900 6300 50  0001 C CNN
F 3 "" H 7900 6300 50  0001 C CNN
	1    7900 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 5050 6350 6300
Wire Wire Line
	6350 6300 7150 6300
Wire Wire Line
	7900 6300 8500 6300
Wire Wire Line
	9050 6300 9050 5900
Connection ~ 7900 6300
Wire Wire Line
	8500 5850 8500 6300
Connection ~ 8500 6300
Wire Wire Line
	8500 6300 9050 6300
Wire Wire Line
	7900 6050 7900 6300
Wire Wire Line
	7900 5550 7750 5550
Wire Wire Line
	7750 5550 7750 5500
Wire Wire Line
	7150 5200 7150 5050
Wire Wire Line
	7150 5050 7450 5050
Wire Wire Line
	7150 5700 7150 6300
Connection ~ 7150 6300
Wire Wire Line
	7150 6300 7900 6300
Wire Wire Line
	8050 5050 8500 5050
Wire Wire Line
	8500 5050 8500 5150
Wire Wire Line
	7900 4400 8500 4400
Wire Wire Line
	8500 4400 8500 5050
Connection ~ 8500 5050
Wire Wire Line
	6350 4650 7150 4650
Wire Wire Line
	7150 4650 7150 5050
Connection ~ 7150 5050
Wire Wire Line
	7150 4650 7600 4650
Wire Wire Line
	7600 4650 7600 4400
Connection ~ 7150 4650
Wire Wire Line
	8500 5050 9050 5050
Wire Wire Line
	9050 5400 9050 5050
Connection ~ 9050 5050
Wire Wire Line
	9050 5050 9300 5050
Wire Wire Line
	8500 5450 8500 5500
Wire Wire Line
	8500 5500 7750 5500
Connection ~ 8500 5500
Wire Wire Line
	8500 5500 8500 5550
Connection ~ 7750 5500
Wire Wire Line
	7750 5500 7750 5350
Text Notes 10700 7000 0    50   ~ 0
ETHENG001\n
Text Notes 7400 7500 0    50   ~ 0
Amplifier\n
Text GLabel 6350 4650 1    50   Output ~ 0
Vpsu
$Comp
L Device:R_US R6
U 1 1 60C60769
P 9450 5050
F 0 "R6" H 9518 5096 50  0000 L CNN
F 1 "5Ω" H 9518 5005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 9490 5040 50  0001 C CNN
F 3 "~" H 9450 5050 50  0001 C CNN
	1    9450 5050
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 5050 9800 5050
Wire Wire Line
	9800 5050 9800 5100
Text GLabel 9800 5100 3    50   Output ~ 0
Vbat
Text HLabel 9800 5050 2    50   Output ~ 0
Vups
$EndSCHEMATC
