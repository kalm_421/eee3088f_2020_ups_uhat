EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7100 6850 0    50   ~ 0
3088 design LED status\nSamuel Pogrund - PGRSAM001\nV1\n
$Comp
L pspice:OPAMP U1
U 1 1 60B9ECB8
P 4500 4200
F 0 "U1" H 4500 3719 50  0000 C CNN
F 1 "OPAMP" H 4500 3810 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-5_Vertical" H 4500 4200 50  0001 C CNN
F 3 "~" H 4500 4200 50  0001 C CNN
	1    4500 4200
	1    0    0    1   
$EndComp
$Comp
L pspice:OPAMP U3
U 1 1 60BA0184
P 6250 4300
F 0 "U3" H 6250 3819 50  0000 C CNN
F 1 "OPAMP" H 6250 3910 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-5_Vertical" H 6250 4300 50  0001 C CNN
F 3 "~" H 6250 4300 50  0001 C CNN
	1    6250 4300
	1    0    0    1   
$EndComp
$Comp
L pspice:OPAMP U2
U 1 1 60BA12AF
P 5400 2550
F 0 "U2" H 5400 2069 50  0000 C CNN
F 1 "OPAMP" H 5400 2160 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-5_Vertical" H 5400 2550 50  0001 C CNN
F 3 "~" H 5400 2550 50  0001 C CNN
	1    5400 2550
	1    0    0    1   
$EndComp
$Comp
L Device:R R1
U 1 1 60BA4528
P 3350 3750
F 0 "R1" V 3143 3750 50  0000 C CNN
F 1 "1k" V 3234 3750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 3280 3750 50  0001 C CNN
F 3 "~" H 3350 3750 50  0001 C CNN
	1    3350 3750
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 60BA58F9
P 3350 4100
F 0 "R2" V 3143 4100 50  0000 C CNN
F 1 "1k" V 3234 4100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 3280 4100 50  0001 C CNN
F 3 "~" H 3350 4100 50  0001 C CNN
	1    3350 4100
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 60BA5BFB
P 4950 3400
F 0 "R5" V 4743 3400 50  0000 C CNN
F 1 "1k" V 4834 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4880 3400 50  0001 C CNN
F 3 "~" H 4950 3400 50  0001 C CNN
	1    4950 3400
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 60BA665D
P 5450 4150
F 0 "R6" V 5243 4150 50  0000 C CNN
F 1 "1k" V 5334 4150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5380 4150 50  0001 C CNN
F 3 "~" H 5450 4150 50  0001 C CNN
	1    5450 4150
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 60BA744C
P 6450 3550
F 0 "R8" V 6243 3550 50  0000 C CNN
F 1 "1k" V 6334 3550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 6380 3550 50  0001 C CNN
F 3 "~" H 6450 3550 50  0001 C CNN
	1    6450 3550
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 60BA7C65
P 5900 2900
F 0 "R7" H 5830 2854 50  0000 R CNN
F 1 "1k" H 5830 2945 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5830 2900 50  0001 C CNN
F 3 "~" H 5900 2900 50  0001 C CNN
	1    5900 2900
	-1   0    0    1   
$EndComp
$Comp
L Device:R R9
U 1 1 60BA8461
P 7900 3550
F 0 "R9" H 7830 3504 50  0000 R CNN
F 1 "1k" H 7830 3595 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 7830 3550 50  0001 C CNN
F 3 "~" H 7900 3550 50  0001 C CNN
	1    7900 3550
	-1   0    0    1   
$EndComp
$Comp
L Device:R R10
U 1 1 60BA8E21
P 8300 3550
F 0 "R10" H 8230 3504 50  0000 R CNN
F 1 "1k" H 8230 3595 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 8230 3550 50  0001 C CNN
F 3 "~" H 8300 3550 50  0001 C CNN
	1    8300 3550
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D1
U 1 1 60BACA4C
P 5900 3350
F 0 "D1" V 5847 3430 50  0000 L CNN
F 1 "Green" V 5938 3430 50  0000 L CNN
F 2 "LED_THT:LED_D3.0mm" H 5900 3350 50  0001 C CNN
F 3 "~" H 5900 3350 50  0001 C CNN
	1    5900 3350
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D2
U 1 1 60BADD0B
P 7900 4650
F 0 "D2" V 7939 4532 50  0000 R CNN
F 1 "RED" V 7848 4532 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 7900 4650 50  0001 C CNN
F 3 "~" H 7900 4650 50  0001 C CNN
	1    7900 4650
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D3
U 1 1 60BAE09D
P 8300 4650
F 0 "D3" V 8339 4532 50  0000 R CNN
F 1 "Blue" V 8248 4532 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 8300 4650 50  0001 C CNN
F 3 "~" H 8300 4650 50  0001 C CNN
	1    8300 4650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 60BAF1E3
P 4400 5050
F 0 "#PWR01" H 4400 4800 50  0001 C CNN
F 1 "GND" H 4405 4877 50  0000 C CNN
F 2 "" H 4400 5050 50  0001 C CNN
F 3 "" H 4400 5050 50  0001 C CNN
	1    4400 5050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 60BAFA8D
P 6100 5100
F 0 "#PWR06" H 6100 4850 50  0001 C CNN
F 1 "GND" H 6105 4927 50  0000 C CNN
F 2 "" H 6100 5100 50  0001 C CNN
F 3 "" H 6100 5100 50  0001 C CNN
	1    6100 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 60BAFEAC
P 5900 3650
F 0 "#PWR05" H 5900 3400 50  0001 C CNN
F 1 "GND" H 5905 3477 50  0000 C CNN
F 2 "" H 5900 3650 50  0001 C CNN
F 3 "" H 5900 3650 50  0001 C CNN
	1    5900 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 60BB0269
P 7850 5200
F 0 "#PWR07" H 7850 4950 50  0001 C CNN
F 1 "GND" H 7855 5027 50  0000 C CNN
F 2 "" H 7850 5200 50  0001 C CNN
F 3 "" H 7850 5200 50  0001 C CNN
	1    7850 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 60BB056F
P 8250 5200
F 0 "#PWR08" H 8250 4950 50  0001 C CNN
F 1 "GND" H 8255 5027 50  0000 C CNN
F 2 "" H 8250 5200 50  0001 C CNN
F 3 "" H 8250 5200 50  0001 C CNN
	1    8250 5200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 60BB17C7
P 3250 2750
F 0 "J1" H 3358 2931 50  0000 C CNN
F 1 "Conn_01x02_Male" H 3358 2840 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-396_A-41791-0002_1x02_P3.96mm_Vertical" H 3250 2750 50  0001 C CNN
F 3 "~" H 3250 2750 50  0001 C CNN
	1    3250 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 3750 3950 3750
Wire Wire Line
	3950 3750 3950 4100
Wire Wire Line
	3500 4100 3950 4100
Connection ~ 3950 4100
Wire Wire Line
	3200 3750 2650 3750
Wire Wire Line
	3200 4100 2650 4100
$Comp
L Device:R R3
U 1 1 60BB8E0C
P 4450 2050
F 0 "R3" H 4520 2096 50  0000 L CNN
F 1 "100" H 4520 2005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4380 2050 50  0001 C CNN
F 3 "~" H 4450 2050 50  0001 C CNN
	1    4450 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 60BB9DE1
P 4450 2650
F 0 "R4" H 4520 2696 50  0000 L CNN
F 1 "900" H 4520 2605 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4380 2650 50  0001 C CNN
F 3 "~" H 4450 2650 50  0001 C CNN
	1    4450 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 1550 4450 1750
$Comp
L power:GND #PWR03
U 1 1 60BBA7F4
P 4450 2800
F 0 "#PWR03" H 4450 2550 50  0001 C CNN
F 1 "GND" H 4455 2627 50  0000 C CNN
F 2 "" H 4450 2800 50  0001 C CNN
F 3 "" H 4450 2800 50  0001 C CNN
	1    4450 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 2200 4450 2450
Wire Wire Line
	4450 2450 5100 2450
Connection ~ 4450 2450
Wire Wire Line
	4450 2450 4450 2500
Wire Wire Line
	4450 1750 5300 1750
Wire Wire Line
	5300 1750 5300 2250
Wire Wire Line
	5300 1750 6150 1750
Connection ~ 5300 1750
Wire Wire Line
	4800 4200 5100 4200
Wire Wire Line
	5150 4200 5150 4150
Wire Wire Line
	5300 4150 5150 4150
Wire Wire Line
	5100 3400 5100 4200
Connection ~ 5100 4200
Wire Wire Line
	5100 4200 5150 4200
Wire Wire Line
	4800 3400 4100 3400
Wire Wire Line
	4100 3400 4100 4100
Wire Wire Line
	3950 4100 4100 4100
Connection ~ 4100 4100
Wire Wire Line
	4100 4100 4200 4100
Wire Wire Line
	4200 4300 3950 4300
Wire Wire Line
	3950 4300 3950 4700
Wire Wire Line
	3950 4700 4400 4700
Wire Wire Line
	4400 4500 4400 4700
Connection ~ 4400 4700
Wire Wire Line
	4400 4700 4400 5050
Wire Wire Line
	6150 4600 6150 4850
Wire Wire Line
	6150 4850 6100 4850
Wire Wire Line
	6100 4850 6100 5100
Wire Wire Line
	5950 4400 5950 4850
Wire Wire Line
	5950 4850 6100 4850
Connection ~ 6100 4850
Wire Wire Line
	5600 4150 5800 4150
Wire Wire Line
	5800 4150 5800 4200
Wire Wire Line
	5800 4200 5950 4200
Wire Wire Line
	5700 2550 5900 2550
Wire Wire Line
	5900 2550 5900 2750
Wire Wire Line
	5900 3050 5900 3200
Wire Wire Line
	5900 3500 5900 3650
Wire Wire Line
	6300 3550 6100 3550
Wire Wire Line
	6100 3550 6100 3950
Wire Wire Line
	6100 3950 5800 3950
Wire Wire Line
	5800 3950 5800 4150
Connection ~ 5800 4150
Wire Wire Line
	6600 3550 6800 3550
Wire Wire Line
	6800 3550 6800 4300
Wire Wire Line
	6800 4300 6550 4300
Wire Wire Line
	6800 4300 7350 4300
Wire Wire Line
	7350 4300 7350 3150
Wire Wire Line
	7350 3150 7900 3150
Wire Wire Line
	7900 3150 7900 3400
Connection ~ 6800 4300
Wire Wire Line
	7900 3700 7900 4500
Wire Wire Line
	7900 4800 7900 5200
Wire Wire Line
	7900 5200 7850 5200
Wire Wire Line
	8300 3700 8300 4500
Wire Wire Line
	8300 4800 8300 5100
Wire Wire Line
	8300 5100 8250 5100
Wire Wire Line
	8250 5100 8250 5200
Wire Wire Line
	8300 3400 8300 3050
Text Label 6150 4000 0    50   ~ 0
VCC
Text Label 4400 3900 0    50   ~ 0
VCC
Text Label 3450 2750 0    50   ~ 0
Vbatt
Text Label 3450 2850 0    50   ~ 0
Vmains
Text Label 2650 3750 0    50   ~ 0
Vmains
Text Label 2650 4100 0    50   ~ 0
Vbatt
Text Label 8300 3050 0    50   ~ 0
Vmains
Text Label 5100 2650 0    50   ~ 0
Vbatt
$Comp
L power:GND #PWR04
U 1 1 60BCA4E7
P 5300 2850
F 0 "#PWR04" H 5300 2600 50  0001 C CNN
F 1 "GND" H 5305 2677 50  0000 C CNN
F 2 "" H 5300 2850 50  0001 C CNN
F 3 "" H 5300 2850 50  0001 C CNN
	1    5300 2850
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0101
U 1 1 60BCC957
P 4450 1550
F 0 "#PWR0101" H 4450 1400 50  0001 C CNN
F 1 "+5V" H 4465 1723 50  0000 C CNN
F 2 "" H 4450 1550 50  0001 C CNN
F 3 "" H 4450 1550 50  0001 C CNN
	1    4450 1550
	1    0    0    -1  
$EndComp
Text Label 6150 1750 0    50   ~ 0
VCC
Wire Wire Line
	4450 1900 4450 1750
Connection ~ 4450 1750
$EndSCHEMATC
